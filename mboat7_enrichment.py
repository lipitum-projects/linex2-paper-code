import pandas as pd
from linex2 import *
import pickle


data_path = "MBOAT7/"

mboat7_dat = pd.read_csv(data_path + "mboat7_data_lm.csv", index_col=0)
mboat7_protein = pd.read_csv(data_path + "mboat7_protein.csv", index_col=0)

mboat7_data = mboat7_dat.divide(mboat7_protein["protein [µg]"], axis="rows")

mboat7_groups = pd.read_csv(data_path + "mboat7_groups.csv", index_col=0).iloc[:,0]
mboat7_fa_file = data_path + "mboat7_fatty_acids.txt"

# Main class
ln_m = LipidNetwork(mboat7_data,
                    mboat7_fa_file,
                    mboat7_groups,
                    lipids_lm_compatible=True,
                    database=("Rhea", "Reactome"),
                    ether_conversions=True
                    )

g, scores, p_values, fig = ln_m.analysis_reaction_network_enrichment("WT HFCD", "KO HFCD",
                                                               penalty_fa_reaction=.5,
                                                               penalty_n_reactions = 2,
                                                               temp=30,
                                                               repetitions=15,
                                                               difference_as_change=True,
                                                               p_value_iterations=5000,
                                                               recompute_edgelist=False,
                                                               plot_fig_size=(8,8),
                                                               correct_n_lipids=True)

pickle.dump(g, open("mboat7_net.p", "wb"))
pickle.dump(scores, open("mboat7_scores.p", "wb"))
pickle.dump(p_values, open("mboat7_pvals.p", "wb"))
pickle.dump(mboat7_data, open("mboat7_data.p", "wb"))
pickle.dump(mboat7_groups, open("mboat7_groups.p", "wb"))
g = pickle.load(open("mboat7_net.p", "rb"))
