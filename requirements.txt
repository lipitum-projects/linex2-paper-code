linex2>=1.0.0
networkx==2.6.2
pandas==1.3.3
numpy==1.20.3
sklearn==1.0.0
statsmodels==0.12.2
matplotlib==3.3.2
seaborn==0.11.1
pyvis==0.1.9
jinja2==3.0.1
tqdm==4.62.1
cairosvg==2.5.2

