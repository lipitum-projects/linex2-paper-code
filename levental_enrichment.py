from enrichment import run_analysis

run_analysis(
    'Levental/Data.csv', 'Levental/Groups.csv',
    'Levental/results', None, [('Undifferentiated _PM_untreated', 'Adipogenic_PM_untreated')],
    fatty_acids='Levental/fatty_acids.txt', lipids_lm_compatible=True,
    plot_args={'correct_n_lipids': True}
)
