# LINEX2 Paper Reproducibility

This entire zip-folder will be public within the next days at https://gitlab.lrz.de/lipitum-projects/linex2-paper-code.

Repository to reproduce the analyses and figures from the [LINEX2 paper](https://www.biorxiv.org/content/10.1101/2022.02.04.479101v1.abstract).

## System requirements
All code in this repository runs on Windows, Mac and Linux and only requires python to be installed.
To install python see https://www.python.org/downloads/ and choose a version >= 3.8.11.

## Required packages

The code was test using python version 3.8.11 and should run with all subsequent versions.<br>
The following packages are required to run the code, tested versions are indicated in brackets.

* linex2 (>=1.0.8)
* networkx (2.6.2)
* pandas (1.3.3)
* numpy (1.20.3)
* sklearn (0.24.2)
* statsmodels(0.12.2)
* matplotlib (3.3.2)
* seaborn (0.11.1)
* pyvis (0.1.9)
* jinja2(3.0.1)
* tqdm (4.62.1)
* cairosvg (2.5.2)

To install all required packages run `pip3 install -r requirements.txt`.

The installation of the cairosvg package requires the [cairo graphics library](https://www.cairographics.org/download/), which is available for all operating systems supported by LINEX (Windows, Mac, Linux).

Please note that converting lipid nomenclature additionally requires the installation of the [LipidLynxX package](https://github.com/SysMedOs/LipidLynxX/tree/master/lynx). If it is not installed
a warning will be shown when using linex2 without name conversion and an error will be thrown when trying to attempt name conversion.

To do so, copy the folder into one the python site-packages folder. If you don't know where the folder is located you can find it by running `python3 -c 'import site; print(site.getsitepackages())'` (this does not work with virtual environments).

## Running linex2
The main class is `LipidNetwork`. For construction, it requires a file path to lipidomics data in a compatible nomenclature (see manuscript for details), all other parameters are optional.
In order to run an enrichment analysis, sample groups are required as well. To run an enrichment analysis, call `analysis_reaction_network_enrichment` on an `LipidNetwork` instance. The function
requires at least two parameters, which represent the groups to compare in the enrichment. These groups have to be present in the sample group annotation provided prior.

### Minimal Example
```python
import pandas as pd
from linex2 import LipidNetwork

# rows are samples, columns are lipids
data = pd.read_csv("data.csv", index_col=0)
# groups is a pd.Series, where indices correspond to those in data
groups = pd.read_csv("groups.csv", index_col=0).iloc[:, 0]
# initializing the LipidNetwork instance
lipid_network = LipidNetwork(data, groups)
# Running the actual enrichment
# graphs is a dictionary containing the enriched subnetwork (value) for each component (key)
# scores is a dictionary containing the score progression of the objective function (value) for each component (key)
# p_values is a dictionary containing the p-value of the best solution (value) for each component (key)
# figure is matplotlib figure object in which each component's solution is plotted in a subplot
graphs, scores, p_values, figure = lipid_network.analysis_reaction_network_enrichment("group1", "group2")
```

## Analyses found in the paper
Please note that the LINEX2 enrichment algorithm is non-deterministic. Therefore, the output might differ from the reported outputs.<br>
To see an example for a computational LINEX workflow, see `enrichment.py`, which holds the analysis function for the network enrichment.<br>
Depending on the number of measured lipids, network generation should take some seconds to a few minutes. Network enrichment should take a few minutes for all iterations.<br>

The output presented in the paper can be found in the respective folders.

All analyses run with the scripts in this repository can also be reproduced using the [webtool](https://exbio.wzw.tum.de/linex).

### AdipoAtlas analyses
* to reproduce the enrichment on the [AdipoAtlas](https://doi.org/10.1016/j.xcrm.2021.100407) data run `python3 adipoatlas_enrichment.py`
* both input data and result files are located in the 'AdipoAtlas' folder

When running the script several warnings will be printed, indicating that 'Car' (i.e. Carnitines) are currently not supported.

The substructure analysis was conducted using the webtool only. To reproduce this analysis, please go to https://exbio.wzw.tum.de/linex/upload,
choose 'LINEX2' and upload the lipidomics data, group annotation and fatty acid file located in the 'AdipoAtlas' folder. The substructure
analysis is the located on the results page in the 'Lipidome Summary' tab.

### Cell-line analyses
* to reproduce the enrichment on the data by [Levental et al.](https://doi.org/10.1126/sciadv.aao1193) data run `python3 levental_enrichment.py`.
* both input data and result files are located in the 'Levental' folder.

### MBOAT 7 analyses
* To reproduce the analysis on the MBOAT7 data from [Thangapandi et al.](http://dx.doi.org/10.1136/gutjnl-2020-320853)
run `python3 mboat7_enrichment.py`. This requires the original data from the authors, which is available upon request.
* To generate the plots from the paper we provide pre-computed results,
which work independent of the original author data.

## Paper figure reproduction
The code to generate the figures presented in the paper is located in paper_figures.py.
Simply running `python3 paper_figures.py` will generate all figures except for figure 1 and
supplementary figures S2 and S3, which are schemas and were generated using inkscape (figs. 1, S3 and BioPan fig. S2).

When running the script several warnings will appear, which do not hamper the results.


