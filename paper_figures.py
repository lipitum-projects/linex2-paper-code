import json
import pickle
import pandas as pd
import numpy as np
import networkx as nx
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import to_rgba
from matplotlib.lines import Line2D
from sklearn.decomposition import PCA
from string import ascii_uppercase
from cairosvg import svg2png
from PIL import Image
from io import BytesIO
from linex2 import *
from linex2 import (
    load_data,
    libAP_nx,
    utils
)
from linex2.utils import lipid_colours as colorscheme


# plt.rcParams['font.family'] = 'sans-serif'
# plt.rcParams['font.sans-serif'] = ['Helvetica']


def read_svg(file: str, **kwargs):
    img = svg2png(file_obj=open(file, 'r'), **kwargs)
    img = Image.open(BytesIO(img))
    return img


def zscore(df: pd.DataFrame):
    scaled = np.zeros(df.shape)
    for i in range(df.shape[0]):
        scaled[i, :] = df.values[i, :] - df.values[i, :].mean()
    scaled = (scaled.T / scaled.std(axis=1)).T
    return pd.DataFrame(scaled, index=df.index, columns=df.columns)


def plot_network(network, axis, positions=None, show_labels=True,
                 node_size=1000, node_colours=None,
                 node_shape_match=('node_molecule_type', 'Enzyme'),
                 label_size=12, enzyme_color="tab:grey",
                 edge_color="tab:grey"):
    if positions is None:
        positions = nx.kamada_kawai_layout(network)
    triangle_nodes = []
    default_nodes = []
    for node in network.nodes:
        if network.nodes[node].get(node_shape_match[0], "None") == node_shape_match[1]:
            triangle_nodes.append(node)
        else:
            default_nodes.append(node)
    if node_colours is None:
        node_colours_ = [colorscheme.get(network.nodes[node]["lipid_class"], "tab:grey")
                         for node in default_nodes]
    else:
        node_colours_ = [node_colours[node] for node in default_nodes]
    nx.draw_networkx_nodes(
        network, positions, ax=axis,
        nodelist=triangle_nodes, node_shape='^',
        node_color=enzyme_color,
        node_size=node_size,
        edgecolors=to_rgba("#D3D3D3")
    )
    nx.draw_networkx_nodes(
        network, positions, ax=axis,
        nodelist=default_nodes,
        node_color=node_colours_,
        node_size=node_size,
        edgecolors=to_rgba("#D3D3D3")
    )
    nx.draw_networkx_edges(
        network, positions, ax=axis,
        edge_color=edge_color
    )
    if show_labels:
        node_labels = {
            node: network.nodes[node].get("representation_id")
            if network.nodes[node].get('node_molecule_type', "None") == "Enzyme"
            else network.nodes[node]['label']
            for node in network.nodes
        }
        nx.draw_networkx_labels(
            network, positions, ax=axis,
            labels=node_labels, font_size=label_size
        )


def plot_pca(data, samples, ax, legend=False, group_name=None, **kwargs):
    pca = PCA(n_components=2)
    reduced_data = pca.fit_transform(data)

    explained_variance = pca.explained_variance_ratio_
    explained_variance = [round(x * 100, 2) for x in explained_variance]

    plot_data = pd.DataFrame(data=reduced_data, columns=["PC1", "PC2"],
                             index=data.index).assign(group=samples)
    if group_name is not None:
        plot_data.rename(columns={'group': group_name})
    if "WT HFCD" in plot_data['group'].values:
        plot_data['group'][plot_data['group'] == "WT HFCD"] = "WT"
        plot_data['group'][plot_data['group'] == "KO HFCD"] = "KO"
    sns.scatterplot(
        x=plot_data['PC1'],
        y=plot_data['PC2'],
        hue=plot_data['group'],
        legend=legend, ax=ax,
        **kwargs
    )
    ax.set_xlabel(f'Principal Component 1 ({explained_variance[0]}%)')
    ax.set_ylabel(f'Principal Component 2 ({explained_variance[1]}%)')


def annotate_subplots(*axes, fontsize=30):
    for ax, label in zip(axes, ascii_uppercase):
        ax.text(-.2, 1.1, label, transform=ax.transAxes,
                fontsize=fontsize, verticalalignment='top',
                fontweight='bold',# fontfamily='sans-serif',
                bbox=dict(facecolor='none', edgecolor='none', pad=0))


def create_legend(palette, nodes=True):
    if nodes:
        return [
            Line2D(
                [0], [0], color="w", marker="o",
                markersize=25,
                markerfacecolor=to_rgba(colour),
                markeredgecolor=to_rgba("#D3D3D3"),
                label=label
            )
            for label, colour in palette.items()
        ]
    return [
        Line2D(
            [0], [0], color=colour, lw=7,
            label=label
        )
        for label, colour in palette.items()
    ]


def str_is_float(label_str):
    try:
        float(label_str)
        return True
    except ValueError:
        return False


def plot_ratios(ratios: dict):
    dfs = []
    for react, df in ratios.items():
        df['Reaction'] = react
        dfs.append(df)
    return pd.concat(dfs)


if __name__ == "__main__":
    # Figure 2
    # Data processing
    # All reactions
    reactome_n_all = load_data.REACTOME_REACTION_DETAILS.shape[0]
    rhea_n_all = len(libAP_nx.flatten([x.split(";") for x in list(load_data.RHEA_REACTION_CURATION['reaction_IDs'])]))
    # Curated_reactions
    ref_lips = parse_lipid_reference_table_dict(load_data.STANDARD_LIPID_CLASSES)
    reactome_cur = make_all_reaction_list_from_reactome(
        load_data.REACTOME_OTHERS_UNIQUE,
        load_data.REACTOME_REACTION_CURATION,
        load_data.REACTOME_REACTION_TO_MOLECULE,
        ref_lips,
        load_data.REACTOME_REACTION_DETAILS
    )
    all_ids = []
    reactome_annotated = 0
    for i in range(len(reactome_cur[0])):
        if reactome_cur[0][i].is_valid(verbose=False):
            all_ids += reactome_cur[0][i].get_enzyme_id().split(";")
            if (len(reactome_cur[0][i].get_uniprot()) > 3) or (len(reactome_cur[0][i].get_gene_name()) > 3):
                reactome_annotated += len(reactome_cur[0][i].get_enzyme_id().split(";"))
    reactome_n_curated = len(set(all_ids))

    rhea_cur = make_all_reaction_list_from_rhea(
        load_data.RHEA_OTHERS_UNIQUE,
        load_data.RHEA_REACTION_CURATION,
        load_data.RHEA_REACTION_TO_MOLECULE,
        ref_lips,
        load_data.RHEA_MAPPING,
        verbose=False
    )

    all_ids = []
    rhea_annotated = 0
    for i in range(len(rhea_cur[0])):
        if rhea_cur[0][i].is_valid(verbose=False):
            all_ids += rhea_cur[0][i].get_enzyme_id().split(";")
            if (len(rhea_cur[0][i].get_uniprot()) > 3) or (len(rhea_cur[0][i].get_gene_name()) > 3):
                rhea_annotated += len(rhea_cur[0][i].get_enzyme_id().split(";"))
    rhea_n_curated = len(set(all_ids))

    org_dict = {}
    for i in range(len(reactome_cur[0])):
        if reactome_cur[0][i].is_valid(verbose=False):
            tmp_ids = reactome_cur[0][i].get_enzyme_id().split(";")
            for org_ids in tmp_ids:
                new_split = org_ids.split("-")[1]
                if new_split in org_dict.keys():
                    org_dict[new_split] += 1
                else:
                    org_dict[new_split] = 1

    combined_reacs = utils.combine_reactions(reactome_cur[0], rhea_cur[0])

    class_dict = {}
    for i in combined_reacs:
        classes_in_reac = []
        if len(i.get_substrates()) > 0:
            classes_in_reac += [x.get_abbr() for x in i.get_substrates()]
        if len(i.get_products()) > 0:
            classes_in_reac += [x.get_abbr() for x in i.get_products()]
        classes_in_reac = list(set(classes_in_reac))

        for y in classes_in_reac:
            if y in class_dict.keys():
                class_dict[y] += 1
            else:
                class_dict[y] = 1

    cls_list = []
    freq_list = []
    for kls in class_dict:
        cls_list.append(kls)
        freq_list.append(class_dict[kls])
    cls_list = np.array(cls_list)
    freq_list = np.array(freq_list)

    # Plot
    # Subplot1
    f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=False, figsize=(13, 4))
    labels = ['Reactome', 'Rhea']
    all_r = [reactome_n_all, rhea_n_all]
    cur_r = [reactome_n_curated, rhea_n_curated]
    ann_r = [reactome_annotated, rhea_annotated]

    x = np.arange(len(labels))  # the label locations
    width = 0.2  # the width of the bars

    rects1 = ax1.bar(x - width, all_r, width, label='Parsed', color='black')
    rects2 = ax1.bar(x, cur_r, width, label='Curated', color='grey')
    rects1 = ax1.bar(x + width, ann_r, width, label='Uniprot/Gene name assigned', color='lightgrey')

    ax1.set_ylabel('Number of reactions')
    ax1.set_title('')
    ax1.set_xticks(x)
    ax1.set_xticklabels(labels)
    ax1.legend()

    orgs = np.array(list(org_dict.keys()))
    frequency = np.array(list(org_dict.values()))
    sorting = np.argsort(frequency)

    ax2.bar(np.arange(len(orgs)), frequency[sorting], color="grey")
    ax2.set_xticks(np.arange(len(orgs)))
    ax2.set_xticklabels(orgs[sorting], rotation=45)

    # Subplot 3
    sorting = np.argsort(freq_list)
    cls_list = cls_list[sorting]
    freq_list = freq_list[sorting]

    red_cls = cls_list[-11:-1]
    red_freq = freq_list[-11:-1]

    ax3.bar(np.arange(len(red_freq)), red_freq, color="grey")
    ax3.set_xticks(np.arange(len(red_freq)))
    ax3.set_xticklabels(red_cls, rotation=45)

    annotate_subplots(ax1, ax2, ax3, fontsize=20)

    plt.savefig("Figures/Figure2.pdf")

    # Figure 4
    # Processing
    # Load network
    subnet = pickle.load(open("MBOAT7/mboat7_net.p", "rb"))['Connected component 0']
    scores = pickle.load(open("MBOAT7/mboat7_scores.p", "rb"))['Connected component 0']

    mboat7_data = pickle.load(open("MBOAT7/mboat7_data.p", "rb"))
    mboat7_groups = pickle.load(open("MBOAT7/mboat7_groups.p", "rb"))

    # Subplot D
    mboat7_ratios = pickle.load(open("MBOAT7/mboat7_ratios.p", "rb"))
    mboat7_ratios_mean = mboat7_ratios.groupby(['variable', 'groups']).mean().reset_index()
    mboat7_ratios_mean['groups'][mboat7_ratios_mean['groups'] == "WT HFCD"] = "WT"
    mboat7_ratios_mean['groups'][mboat7_ratios_mean['groups'] == "KO HFCD"] = "KO"

    mboat7_ratios_mean = mboat7_ratios_mean.rename(columns={"groups": "Genotype"})
    # Subplot E
    hfcd_samples = mboat7_groups[mboat7_groups.str.contains("HFCD")]

    # Subplot F
    subnet_nodes = [x for x in list(subnet.nodes) if not x.startswith("Enzyme")]

    # Plot function
    # actual plotting
    plt.close('all')
    fig = plt.figure(figsize=(12, 12))

    ax1 = plt.subplot2grid((14, 6), (0, 0), colspan=6, rowspan=5)
    ax2 = plt.subplot2grid((14, 6), (5, 0), colspan=3, rowspan=5)
    ax3 = plt.subplot2grid((14, 6), (5, 3), colspan=3, rowspan=5)
    ax4 = plt.subplot2grid((14, 6), (11, 0), colspan=2, rowspan=5)
    ax5 = plt.subplot2grid((14, 6), (11, 2), colspan=2, rowspan=5)
    ax6 = plt.subplot2grid((14, 6), (11, 4), colspan=2, rowspan=5)

    plt.subplots_adjust(wspace=0, hspace=0)

    # Subplot A
    ax1.imshow(read_svg("Figures/enrichment.svg"))
    ax1.axis('off')

    # ###
    # Network is available in 'subnet'
    # ###
    # Subplot B
    plot_network(subnet, axis=ax2)
    ax2.axis('off')

    # Subplot C
    ax3.plot(scores)
    ax3.set_xlabel("Iterations")
    ax3.set_ylabel("Score")
    ax3.set_title("Score optimization")

    palette = {"WT": "#FF7F2A", "KO": "#00CCFF"}
    # Subplot D
    sns.histplot(ax=ax4,
                 data=mboat7_ratios_mean,
                 x="value",
                 hue="Genotype",
                 element="poly",
                 stat="count",
                 palette=palette
                 )
    ax4.set_xlabel('Reaction ratio')
    ax4.set_ylabel('Count')

    # Subplot E
    plot_pca(mboat7_data.loc[hfcd_samples.index, :], mboat7_groups, ax5, palette=palette)
    ax5.set_title("All lipids")

    # Subplot F
    plot_pca(mboat7_data.loc[hfcd_samples.index, subnet_nodes], mboat7_groups, ax6, palette=palette)
    ax6.set_title("Lipids from enrichment")

    annotate_subplots(ax1, ax2, ax3, ax4, ax5, ax6)

    plt.tight_layout()
    plt.savefig("Figures/Figure4.pdf")
    plt.close(fig)

    # Figure 5
    palette = {"Lean": "#FF7F2A", "Obese": "#00CCFF"}

    # Subfigure A
    subnetwork = json.load(
        open("AdipoAtlas/Obese_vs_Lean.json", 'r'),
    )
    adipo_subnet = nx.Graph()
    for node, data in subnetwork['nodes'].items():
        adipo_subnet.add_node(node, **data)
    for edge in subnetwork['edges']:
        adipo_subnet.add_edge(*edge)

    # Subfigure C
    adipo_substruct = zscore(pd.read_csv("AdipoAtlas/substructure_data.csv", index_col=0))
    adipo_groups = pd.read_csv("AdipoAtlas/Groups.csv", index_col=0).iloc[:, 0]
    adipo_best = []
    with open("AdipoAtlas/substructure_best.csv", "r") as file:
        for line in file:
            adipo_best.append(line.strip())
    adipo_best_substructs = adipo_substruct.loc[adipo_best, :]

    # Plotting
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(figsize=(20, 20), ncols=2, nrows=2)

    # A
    plot_network(adipo_subnet, ax1)
    ax1.axis('off')

    # B
    # LION enrichment
    ax2.imshow(read_svg("Figures/Graph.svg"))
    ax2.axis('off')

    # NOTE: both of these plots have to be loaded from a pdf
    #       since the multiplotting of seaborn is not possible
    #       on a single axis
    # C
    # TODO: why are the plots cut off when we plot them like this?
    ax3.imshow(read_svg("Figures/AdipoAtlas_TotalRatios.svg"))
    ax3.axis("off")
    # D
    ax4.imshow(read_svg("Figures/AdipoAtlas_EnrichmentRatios.svg"))
    ax4.axis("off")

    annotate_subplots(ax1, ax2, ax3, ax4)

    plt.tight_layout()
    plt.savefig("Figures/Figure5.pdf")
    plt.close(fig)

    # Figure S4
    adipo_data = zscore(pd.read_csv("AdipoAtlas/Data.csv", index_col=0))

    fig, (ax1, ax2) = plt.subplots(figsize=(10, 5), ncols=2)

    plot_pca(adipo_data.T, adipo_groups, ax1, palette=palette, legend=True, group_name="Group")
    plot_pca(adipo_substruct.T, adipo_groups, ax2, palette=palette)

    annotate_subplots(ax1, ax2, fontsize=25)


    plt.tight_layout()
    plt.savefig("Figures/FigureS4.pdf")
    plt.close(fig)

    # NOTE: we do not need Figure 3 here => it's already the figure as is
    # Figure 3
    # loading and converting network
    # NOTE: we modified the network from the webtool to not include the
    #       lipid names for visibility
    adipo_linex = json.load(open("AdipoAtlas/LINEXnetwork.json", "r"))
    adipo_net = nx.Graph()
    for attrs in adipo_linex['nodes']:
        adipo_net.add_node(attrs['id'], **attrs)
    for edge in adipo_linex['edges']:
        adipo_net.add_edge(edge['from'], edge['to'], **edge)
    # formatting node attributes
    node_pos = {node_: (attrs['x'], attrs['y']) for node_, attrs in adipo_net.nodes(data=True)}
    edge_cols = {(src, tgt): attrs['color'] for src, tgt, attrs in adipo_net.edges(data=True)}

    edge_pal = {edge['label']: edge['color']
                for edge in adipo_linex['legendEdges']['colour']
                if not edge['hidden']}

    # Figure S5
    # subsetting to
    # DG/TG/PC/PE nodes
    target_classes = {'DG', 'TG', 'PC', 'PE'}
    # this is a workaround since we don't have the lipid class
    # annotation in the json download
    subnodes = [node_ for node_ in adipo_net.nodes
                if adipo_net.nodes[node_]['label'].split("(")[0] in target_classes
                or adipo_net.nodes[node_]['label'].startswith("Reaction")]

    adipo_subnet = adipo_net.subgraph(subnodes).copy()
    adipo_subnet.remove_nodes_from([node_ for node_, degree in adipo_subnet.degree
                                    if degree < 2])
    node_cols = {
        node: colorscheme.get(adipo_subnet.nodes[node]['label'].split("(")[0], "black")
        for node in adipo_subnet.nodes
    }

    # plotting
    fig, ax = plt.subplots(figsize=(10, 10))

    plot_network(
        adipo_subnet, ax, node_pos, True, 200, node_cols,
        ('shape', 'triangle'), 5, "black",
        edge_color=edge_cols.values()
    )
    ax.axis('off')

    palette = {lclass: colorscheme[lclass] for lclass in target_classes}

    plt.gca().add_artist(
        plt.legend(
            handles=create_legend(palette),
            loc="upper left",
            bbox_to_anchor=(0, 1),
            labelspacing=1, title_fontsize=21, fontsize=15,
            framealpha=.5, edgecolor="w"
        )
    )
    plt.gca().add_artist(
        plt.legend(
            handles=create_legend(edge_pal, nodes=False),
            loc="upper right",
            bbox_to_anchor=(1, 1),
            labelspacing=1, title_fontsize=21, fontsize=15,
            framealpha=.5, edgecolor="w"
        )
    )

    plt.tight_layout()
    plt.savefig("Figures/FigureS5.pdf")
    plt.close(fig)

    # Figure S1
    # subfigure A
    subnetwork = json.load(
        open("Levental/Undifferentiated _PM_untreated_vs_Adipogenic_PM_untreated_Connected component 0.json")
    )
    diff_subnet = nx.Graph()
    for node, data in subnetwork['nodes'].items():
        diff_subnet.add_node(node, **data)
    for edge in subnetwork['edges']:
        diff_subnet.add_edge(*edge)

    # plotting
    fig, (ax1, ax2) = plt.figure(figsize=(16, 9), ncols=2)

    plt.subplots_adjust(wspace=0, hspace=0)

    ax1.imshow(read_svg('Figures/EnrichmentFullData.svg'))
    ax1.axis('off')

    plot_network(diff_subnet, ax2, node_size=500,
                 label_size=7)
    ax2.axis('off')

    annotate_subplots(ax1, ax2)

    plt.tight_layout()
    plt.savefig("Figures/FigureS1.pdf", dpi=300)
    plt.close(fig)

    # Figure from the original draft => to go into supplement
    fig, (ax1, ax2) = plt.subplots(figsize=(10, 15), nrows=2)
    # A
    cl_data = pd.read_csv("AdipoAtlas/chainlength_TG.csv", index_col=0).sort_values("variable")

    ax1.plot(cl_data['variable'], cl_data['FC'])
    ax1.scatter(cl_data['variable'], cl_data['FC'])
    ax1.set_ylim(-3, 3)
    ax1.set_xlabel("Chain Length")
    ax1.set_ylabel("Fold Change")

    # B
    # clustering is already pre-computed in webtool
    webtool_data = json.load(open("AdipoAtlas/best_heatmap_data.json", "r"))
    sample_order = webtool_data[0]['x']
    feature_order = webtool_data[0]['y']

    heatmap = sns.heatmap(
        adipo_best_substructs.loc[feature_order, sample_order],
        cbar_kws={'orientation': 'vertical', 'fraction': .1, 'pad': .025},
        ax=ax2, cmap="viridis"
    )
    ax2.set_xticklabels(adipo_groups[sample_order])

    annotate_subplots(ax1, ax2)

    plt.tight_layout()
    plt.savefig("Figures/FigureS6.pdf", dpi=300)
    plt.close(fig)
