from enrichment import *


adipo_net = run_analysis(
    'AdipoAtlas/Data.csv', 'AdipoAtlas/Groups.csv',
    'AdipoAtlas/CorrectedResults', None, [('Obese', 'Lean')],
    lipids_lm_compatible=True, ether_conversions=False,
    fatty_acids='AdipoAtlas/fatty_acids.txt',
    plot_args={'correct_n_lipids': True, 'difference_as_change': True,
               'save_figs_to_pickle': True}
)


cl_data = adipo_net.analysis_chain_length(control_condition="Lean", data_is_log=False, return_data=True)
cl_data['TG'].to_csv("AdipoAtlas/chainlength_TG.csv")
