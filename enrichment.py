import os
import json
import pickle
import numpy as np
import pandas as pd
import networkx as nx
from tqdm import tqdm
from itertools import combinations
from linex2 import LipidNetwork
from linex2.utils import plot_template
import matplotlib.pyplot as plt


def plot_scores(ax, data, key):
    ax.plot(np.arange(len(data)), data)
    ax.scatter(np.arange(len(data)), data)
    ax.set(xlabel='Iteration', ylabel='Score', title=key)


def graph_to_dict(graph: nx.Graph) -> dict:
    return {
        'nodes': dict(graph.nodes(data=True)),
        'edges': [[src, tgt] for src, tgt in graph.edges()]
    }


def compute_enrichment(network: LipidNetwork, folder: str, group_comparisons: list = None,
                       save_figs_to_pickle: bool = False, **kwargs):
    n_cols = kwargs.pop('plot_n_col', 2)
    figsize = kwargs.pop('figsize', (16, 9))
    if group_comparisons is None:
        group_comparisons = combinations(network.unique_groups, 2)
    for group_comp in tqdm(group_comparisons, desc='Enrichment'):
        graphs, scores, pvalues, plot = network.analysis_reaction_network_enrichment(
            group1=group_comp[0], group2=group_comp[1], plot_scores=False,
            plot_n_col=n_cols, **kwargs
        )
        plot_file = f'{group_comp[0]}_vs_{group_comp[1]}.pdf'
        plot.savefig(os.path.join(folder, 'network_' + plot_file))
        plt.close(plot)

        score_plot = plot_template(
            scores, plot_scores, plot_n_col=n_cols, plot_fig_size=figsize
        )
        score_plot.savefig(os.path.join(folder, 'score_' + plot_file))
        plt.close(score_plot)

        json_base = f'{group_comp[0]}_vs_{group_comp[1]}'
        for comp, graph in graphs.items():
            print(comp)
            enzymes = [node for node in graph.nodes
                       if graph.nodes[node]['node_molecule_type'] == 'Enzyme']
            if enzymes:
                enrich_fig, enrich_ratios = network.analysis_enrichment_ratio_plots(
                    graph, select_groups=group_comp, as_boxplot=True,
                    distribution_per_sample=False, fa_specific=False,
                    z_scores=True
                )
                if save_figs_to_pickle:
                    plot_ratios = plot_file.replace(".pdf", ".pickle")
                    pickle.dump(
                        enrich_ratios, open(os.path.join(folder, f'enrichment_ratios_{comp}_' + plot_ratios), "wb")
                    )
                enrich_fig.savefig(os.path.join(folder, f'enrichment_ratios_{comp}_' + plot_file))
                plt.close(enrich_fig)
                full_fig, full_ratios = network.analysis_ratio_distribution(
                    select_groups=group_comp, enzyme_subset=enzymes,
                    as_boxplot=True, distribution_per_sample=False,
                    fa_specific=False, z_scores=True
                )
                if save_figs_to_pickle:
                    plot_ratios = plot_file.replace(".pdf", ".pickle")
                    pickle.dump(
                        full_ratios, open(os.path.join(folder, f'total_ratios_{comp}_' + plot_ratios), "wb")
                    )
                full_fig.savefig(os.path.join(folder, f'total_ratios_{comp}_' + plot_file))
                plt.close(full_fig)

            json_path = os.path.join(folder, f'{json_base}_{comp}.json')
            json.dump(graph_to_dict(graph), open(json_path, 'w'), indent=4)


def run_analysis(
    data: str, groups: str, folder: str, network: LipidNetwork = None,
    group_comparisons: list = None, plot_args: dict = None,
    lipids_are_columns=False, **kwargs
):
    lipid_data = pd.read_csv(data, index_col=0)
    sample_groups = pd.Series(pd.read_csv(groups, index_col=0).iloc[:, 0])

    return_network = False
    if network is None:
        # this is the main class for LINEX 2
        network = LipidNetwork(
            data=lipid_data, groups=sample_groups,
            lipids_are_columns=lipids_are_columns,
            **kwargs
        )
        return_network = True
    else:
        network.data = lipid_data if lipids_are_columns else lipid_data.T
        network.groups = sample_groups
        network.unique_groups = sample_groups.unique()

    if not os.path.exists(folder):
        os.mkdir(folder)
    if plot_args is None:
        plot_args = {}
    compute_enrichment(network, folder, group_comparisons, **plot_args)

    if return_network:
        return network

